#-------------------variables------------------#
DOCKER = docker
DOCKER_RUN = $(DOCKER) run
DOCKER_BUILD = $(DOCKER) build
DOCKER_COMPOSE = docker compose
DOCKER_COMPOSE_UP = $(DOCKER_COMPOSE) up -d
DOCKER_COMPOSE_BUILD = $(DOCKER_COMPOSE) up --build
DOCKER_COMPOSE_DOWN = $(DOCKER_COMPOSE) down
DOCKER_COMPOSE_STOP = $(DOCKER_COMPOSE) stop

PROJECT_NAME = fruitpay-makefile
#----------------------------------------------#

dbuild:
	$(DOCKER_BUILD) -t $(PROJECT_NAME) .
dbuild-nocache:
	$(DOCKER_BUILD) --no-cache -t $(PROJECT_NAME) .
cup:
	$(DOCKER_COMPOSE_UP)
cbuild:
	$(DOCKER_COMPOSE_BUILD)
cdown:
	$(DOCKER_COMPOSE_DOWN)
cstop:
	$(DOCKER_COMPOSE_STOP)