# mengambil atau menggunakan image dari docker registry dengan nasqueron/nginx-php-fpm
FROM nasqueron/nginx-php-fpm
# membuat direktori folder app
RUN mkdir /app
# menduplikasi semua ke dalam folder /app
COPY . . /app/
#  working directory is set to /app
WORKDIR /app
# mengizinkan semua untuk melakukan apapun pada file
RUN chmod 777 /app/order.txt
# menaruh konfigurasi default ke dalam /etc/nginx/sites-enabled/
COPY ./default /etc/nginx/sites-enabled/